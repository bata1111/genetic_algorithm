import numpy
import random as my_rand


def calculate(a, print_result=False):
    x_temp = a[:7]
    y_temp = a[7:]
    x_temp = x_temp[::-1]
    y_temp = y_temp[::-1]
    x_fit = 0
    y_fit = 0
    for temp_calculate1 in range(len(x_temp)):
        if temp_calculate1 == 6:
            if x_temp[6] == 1 and not x_fit == 0:
                x_fit *= -1
            if y_temp[6] == 1 and not y_fit == 0:
                y_fit *= -1
        else:
            y_fit += (y_temp[temp_calculate1] * (pow(2, temp_calculate1)))
            x_fit += (x_temp[temp_calculate1] * (pow(2, temp_calculate1)))

    ff = fitness_function(x_fit, y_fit)
    if print_result:
        print(f' x = {x_fit}, y = {y_fit} f(x,y): {ff}')
    return ff


def fitness_function(x_fit, y_fit):
    return pow(x_fit, 2) + pow(y_fit, 2)


np = 10
m = 15
mr = 0.1
best_number = 0
step = 0
x = numpy.random.randint(2, size=(np, m - 1))
C = numpy.empty([0, m - 1])

while True:
    step += 1
    for c in range(int(np / 2)):
        temp = (c - 1) * 2 + 1
        p1 = x[temp]
        p2 = x[temp + 1]
        cp = numpy.random.randint(2, m)
        c1 = numpy.array(numpy.append(p1[:cp], p2[cp:]))
        c2 = numpy.array(numpy.append(p2[:cp], p1[cp:]))
        C = numpy.append(C, [c1], axis=0)
        C = numpy.append(C, [c2], axis=0)

    # mutation
    nc = C.shape[0]
    for i in range(nc):
        if my_rand.random() < mr:
            mp = numpy.random.randint(m - 1)
            C[i][mp] = (C[i][mp] + 1) % 2

    #  selection

    NP = numpy.vstack([x, C])
    values = []

    for i in NP:
        values.append((i, calculate(i)))

    values = sorted(values, key=lambda my_list: my_list[1])

    temp = values[:10]
    tempList = []
    for i in range(np):
        tempList.append(temp[i][0])
    x = numpy.array(tempList)
    print(f'--------------- step {step}  -----------------')
    for u in x:
        calculate(u, True)
    print(x)
    if values[9][-1] == 0:
        break
